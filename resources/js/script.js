$(document).ready(function() {
    
    
// trigger function for sticky nav to work    
    $('.js-hook').waypoint(function(direction) {

        if(direction == "down") {
            $("nav").addClass("sticky");
        }
        else {
            $("nav").removeClass("sticky");
        }
    
    
    }, {
        offset: '60px;'
    });



//main button scroll
    $('.scroll-to').click(function() {

    $('html, body').animate({scrollTop: $('.im-hungry').offset().top}, 1000);
    
    });


// feature button scroll 
    $('.scroll-to-features').click(function() {

        $('html, body').animate({scrollTop: $('.js-hook').offset().top}, 1000);
        
        });


                /****SOMETHING SCROOLING ****************************** */
        // Select all links with hashes
$('a[href*="#"]')
// Remove links that don't actually link to anything
.not('[href="#"]')
.not('[href="#0"]')
.click(function(event) {
  // On-page links
  if (
    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
    && 
    location.hostname == this.hostname
  ) {
    // Figure out element to scroll to
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    // Does a scroll target exist?
    if (target.length) {
      // Only prevent default if animation is actually gonna happen
      event.preventDefault();
      $('html, body').animate({
        scrollTop: target.offset().top
      }, 1000, function() {
        // Callback after animation
        // Must change focus!
        var $target = $(target);
        $target.focus();
        if ($target.is(":focus")) { // Checking if the target was focused
          return false;
        } else {
          $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
          $target.focus(); // Set focus again
        };
      });
    }
  }
});  /*** smooth scrolling ends here */



//fade in animiation setup

$('.js-wp-1').waypoint(function(direction) {
    
    $('.js-wp-1').addClass('animated fadeIn');

}, {
    offset: '50%'
});   


//zoom in iphone animiation setup

$('.js-wp-2').waypoint(function(direction) {
    
    $('.js-wp-2').addClass('animated bounceInUp');

}, {
    offset: '60%'
});


//zoom right for steps animiation setup

$('.js-wp-3').waypoint(function(direction) {
    
    $('.js-wp-3').addClass('animated bounceInRight');

}, {
    offset: '60%'
});


//wobble for permium box

$('.js-wp-4').waypoint(function(direction) {
    
    $('.js-wp-4').addClass('animated wobble');

}, {
    offset: '30%'
});






}); // main ends here


